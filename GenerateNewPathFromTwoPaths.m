function betterFireflies =  GenerateNewPathFromTwoPaths
global startP goalP probabilityThresholdOfEfficiency numOfFireflies
global coloredMap invMap numOfRows numOfColumns robotMap
global probabilisticMap initialFireflies symmetric origionPointOfWindow
global lineAngle heightOfWindow

betterFireflies{1} = [];

text(startP(2),startP(1),'*', ...
    'HorizontalAlignment','center', ...
    'Color', [0.8 0.33 0], ...
    'FontSize',14);
betterFireflies{1} = [startP(1);startP(2)];
pushedPointToTheNewPath = [startP(1);startP(2)];
previousPushedPointToTheNewPath = [startP(1);startP(2)];
pathA = initialFireflies{1};
pathB = initialFireflies{2};
middlePointsOfA = FindMiddlePointOfSegment(pathA);
middlePointsOfB = FindMiddlePointOfSegment(pathB);
lenB = length(pathB);
idBA = zeros(1,lenB);
valA = zeros(1, lenB);

for j = 2:lenB-1
    % Find the nearest segmentA from segmentB
    [tmp1, tmp2] = min(pdist2(middlePointsOfB(:,j)',...
        middlePointsOfA(:,2:end-1)'));
    valA(j) = tmp1;
    idBA(j) = tmp2+1;
    
    
    % Direction of window expansion
    directionFlag = sign((pathB(1,j) - middlePointsOfB(1,j))...
        * (middlePointsOfA(2,idBA(j))- middlePointsOfB(2,j)) - ...
        (pathB(2,j) - middlePointsOfB(2,j))...
        * (middlePointsOfA(1,idBA(j))- middlePointsOfB(1,j)));
    if directionFlag == 1
        symmetric = 1; % 1 --> Right-handed Asymmetric Window
        
    elseif directionFlag == -1
        symmetric = 2; % 2 --> Left-handed Asymmetric Window
        
    elseif directionFlag == 0 %%% =======PLEASE REVISE THIS CASE=======
        % Case of identical points
        symmetric = 3; % 3 --> Do Not Expand or even Construct a Window
    end
    
    % Construct window
    lineAngle = atan2(pathA(2, idBA(j)) - pathA(2, idBA(j)-1),...
        pathA(1, idBA(j)) - pathA(1, idBA(j)-1));
    
    % Add a new more attractive point to the new path
    origionPointOfWindow = pathB(:,j)';
    previousPoint = [startP(1);startP(2)];
    prePreviousPushedPointToTheNewPath = previousPushedPointToTheNewPath;
    previousPushedPointToTheNewPath = pushedPointToTheNewPath;
    [pushedPointToTheNewPath, previousPoint, isGoalArrived] =...
        AddNewFitterPointToTheNewSuggestedPath(previousPoint, previousPushedPointToTheNewPath...
        ,prePreviousPushedPointToTheNewPath);
    text(pushedPointToTheNewPath(2),...
        pushedPointToTheNewPath(1),'*', ...
        'HorizontalAlignment','center', ...
        'Color', [0.8 0.33 0], ...
        'FontSize',14);
    drawnow;
    betterFireflies{1} =...
        cat(2, betterFireflies{1},...
        [pushedPointToTheNewPath(1);pushedPointToTheNewPath(2)]);
end

text(goalP(2),goalP(1),'*', ...
    'HorizontalAlignment','center', ...
    'Color', [0.8 0.33 0], ...
    'FontSize',14);
betterFireflies{1} =...
    cat(2, betterFireflies{1}, [goalP(1);goalP(2)]);
end
