function [pushedPointToTheNewPath, previousPoint, isGoalArrived] =...
    AddNewFitterPointToTheNewSuggestedPath(previousPoint,previousPushedPointToTheNewPath...
    , prePreviousPushedPointToTheNewPath)
global startP goalP probabilityThresholdOfEfficiency numOfFireflies
global coloredMap invMap numOfRows numOfColumns robotMap
global probabilisticMap initialFireflies symmetric origionPointOfWindow
global lineAngle heightOfWindow widthOfWindow 

helperCounter = 1;
xCorners = ones(1,5);
yCorners = ones(1,5);
probabilityDensityIdx = [];

breviousHeightOfWindow = heightOfWindow;
breviousWidthOfWindow = widthOfWindow;
[window, ~, xCorners, yCorners, borderKeeperFlag] =...
    ConstructWindow(robotMap, origionPointOfWindow,startP, goalP,...
    symmetric, lineAngle, xCorners, yCorners, heightOfWindow, widthOfWindow,breviousHeightOfWindow, breviousWidthOfWindow);

%figure(2);
% imshow(window);
% drawnow
% hold on;

% apply the window on the probabilistic map
regionOfInterest =  probabilisticMap .* window;
% imshow(regionOfInterest);
isGoalArrived = regionOfInterest(goalP(1),goalP(2));
effeciencyOfROI = sum(sum(regionOfInterest));
numOfWindowCells = sum(sum(window));
expandingHeightCounter = 1;

epsilonFlag = false;
probabilityThresholdOfEfficiencyInitial = probabilityThresholdOfEfficiency;
while epsilonFlag == false
    while (effeciencyOfROI < (probabilityThresholdOfEfficiency*numOfWindowCells)) || (length(find(regionOfInterest>0))==0)
        
        epsilonFlag=false;
        breviousHeightOfWindow = heightOfWindow;
        breviousHeightOfWindow = widthOfWindow;
        heightOfWindow = 1.3*heightOfWindow;% Expand the height of the window
        
        if probabilityThresholdOfEfficiency < 0.7 * probabilityThresholdOfEfficiencyInitial
            widthOfWindow = 1.3 * widthOfWindow;
        end
        
        % Construct the window
        [window, ~, xCorners, yCorners, borderKeeperFlag] =...
            ConstructWindow(robotMap, origionPointOfWindow,startP, goalP,...
            symmetric, lineAngle, xCorners, yCorners, heightOfWindow,...
            widthOfWindow, breviousHeightOfWindow, breviousWidthOfWindow);
        if borderKeeperFlag == 1 ||...
                (probabilityThresholdOfEfficiency...
                <=0.3*probabilityThresholdOfEfficiencyInitial...
                && regionOfInterest>0)
            break;
        end
        
        % apply the window(mask)on the probabilistic map
        regionOfInterest =  probabilisticMap .* window;
%         imshow(regionOfInterest);
%         drawnow;
        
        effeciencyOfROI = sum(sum(regionOfInterest));
        if isempty(effeciencyOfROI)
            continue
        end
        
        numOfWindowCells = sum(sum(window));
        expandingHeightCounter = expandingHeightCounter +1;
        
        if expandingHeightCounter >= 3
            probabilityThresholdOfEfficiency =...
                probabilityThresholdOfEfficiency * 8/10;
            expandingHeightCounter = 1;
        end
    end
    
    % Apply the Probability Density Function (PD) and Choose Random Point
    if isGoalArrived>0
        newStartPointIdx = sub2ind([numOfRows, numOfColumns], goalP);
        origionPointOfWindow = regionOfInterest(goalP(1),goalP(2));
        pushedPointToTheNewPath = newStartPointIdx;
        previousPushedPointToTheNewPath = pushedPointToTheNewPath;
        epsilonFlag = true;
    else
        
        normalizedProbability = regionOfInterest ./ effeciencyOfROI;
        minNormalizedProbability =...
            min(normalizedProbability(normalizedProbability>0));

        [sortedProbs, probsIdx] = sort(normalizedProbability(:),'descend');
        probabilityDensityRepetition =...
            sortedProbs ./ minNormalizedProbability;
        zeroIdx = find(sortedProbs==0,1);
        for i=1:zeroIdx-1
            probabilityDensityIdx = cat(1, probabilityDensityIdx,...
                (repmat(probsIdx(i), 1,...
                round(probabilityDensityRepetition(i))))');
        end
        
        while epsilonFlag==false
            Idx = find(randi(length(probabilityDensityIdx))<...
                cumsum(1:length(probabilityDensityIdx)),1);
            newStartPointIdx = probabilityDensityIdx(Idx);
            [newStartPointTemp(1),newStartPointTemp(2)] =...
                ind2sub([numOfRows, numOfColumns],...
                newStartPointIdx);
            if previousPoint == startP
                epsilon = mod(atan2(newStartPointTemp(2) -...
                    previousPushedPointToTheNewPath(2),...
                    newStartPointTemp(1) - previousPushedPointToTheNewPath(1)) -...
                    (lineAngle), 2*pi);
            else
                epsilon =...
                    mod( atan2( det([newStartPointTemp(1) - previousPushedPointToTheNewPath(1),...
                    previousPushedPointToTheNewPath(1) - prePreviousPushedPointToTheNewPath(1);...
                    newStartPointTemp(2) - previousPushedPointToTheNewPath(2),...
                    previousPushedPointToTheNewPath(2) - prePreviousPushedPointToTheNewPath(2)]) ,...
                    dot([newStartPointTemp(1) - previousPushedPointToTheNewPath(1),...
                    newStartPointTemp(2) - previousPushedPointToTheNewPath(2)],...
                    [previousPushedPointToTheNewPath(1) - prePreviousPushedPointToTheNewPath(1),...
                    previousPushedPointToTheNewPath(2) - prePreviousPushedPointToTheNewPath(2)])), 2*pi );
                
            end
            
            helperCounter = helperCounter + 1;
            
            if ~(epsilon > pi/2 && epsilon < 3*pi/2)
                pushedPointToTheNewPath = newStartPointTemp;
                epsilonFlag = true;
                helperCounter = 1;
                disp(['epsilon = ' num2str(rad2deg(mod(epsilon,2*pi)))]);
                
            end
            
            if helperCounter>=5 
                probabilityThresholdOfEfficiency =...
                    max(probabilityThresholdOfEfficiency - 0.1,0.1);
                helperCounter = 1;
                break;
            end
        end
    end
end
previousPoint = origionPointOfWindow;

end