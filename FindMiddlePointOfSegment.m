function middlePoint = FindMiddlePointOfSegment(path)

firstPoint = path(:,1:end-1);
lastPoint = path(:,2:end);
% x0 = (x1 + x2)/2
middlePoint(1,:) = (firstPoint(1,:) + lastPoint(1,:))/2;

% y0 = (y1 + y2)/2
middlePoint(2,:) = (firstPoint(2,:) + lastPoint(2,:))/2;

end