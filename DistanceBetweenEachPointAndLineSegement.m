function [distance, slope] = DistanceBetweenEachPointAndLineSegement(sizeOfMap, startPoint, goalPoint)

startPt = cat(3, repmat(startPoint(1),sizeOfMap(1),sizeOfMap(2)),repmat(startPoint(2),sizeOfMap(1),sizeOfMap(2)));
goalPt = cat(3, repmat(goalPoint(1),sizeOfMap(1),sizeOfMap(2)),repmat(goalPoint(2),sizeOfMap(1),sizeOfMap(2)));

% build the map coordinates grid
[rows,cols] = ndgrid(1:sizeOfMap(1),1:sizeOfMap(2));
mapSubscript = cat(3, rows, cols);

% Slope calculation
slope = (goalPt(:,:,2) - startPt(:,:,2)) ./ (goalPt(:,:,1) - startPt(:,:,1));

% intersection point between the perpendicular and the line
if slope(1,1) == inf
    slope = 200;
elseif slope(1,1) == 0
    slope = 0.05;
elseif slope(1,1) == -Inf
    slope = -200;
end


% Intersection points with the Start-Goal Line
xIntersection = (slope ./ (1 + slope.^2)).*(mapSubscript(:,:,2) + mapSubscript(:,:,1) ./ slope + slope .* startPt(:,:,1) - startPt(:,:,2));
yIntersection = -xIntersection ./ slope + mapSubscript(:,:,2) + mapSubscript(:,:,1) ./ slope;

% find the disance between each point on the map and the Start-Goal Segment
distance = sqrt((mapSubscript(:,:,1) - xIntersection).^2 + (mapSubscript(:,:,2) - yIntersection).^2);

end