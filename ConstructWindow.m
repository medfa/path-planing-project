function [window, slopeAngle, xCorners, yCorners, borderKeeperFlag] =...
    ConstructWindow(robotMap, startPoint,initialStart, goalPoint,...
    symmetric,lineAngle, xCorners, yCorners, heightOfWindow,...
    widthOfWindow, breviousHeightOfWindow, breviousWidthOfWindow)

% symmetric: 3 --> Symmetric Window, 1 --> Right-handed Asymmetric Window,
% 2 --> Left-handed Asymmetric Window

if symmetric == 1 || symmetric == 2
    slopeAngle = lineAngle;
else
    %slopeAngle = atan2(goalPoint(2) - initialStart(2), goalPoint(1) - initialStart(1));
    slopeAngle = atan2(goalPoint(2) - startPoint(2), goalPoint(1) - startPoint(1));
end

%% find X & Y coordinates of the window corners

% xRD, yRD: x & y of the Right-Down corner respectively
% xLU, yLU: x & y of the Left-Upper corner respectively
% and so on...
if symmetric == 3
    xRD = startPoint(1) + heightOfWindow * cos(-pi/2 + slopeAngle);
    yRD = startPoint(2) + heightOfWindow * sin(-pi/2 + slopeAngle);
    xLD = startPoint(1) + heightOfWindow * cos(pi/2 + slopeAngle);
    yLD = startPoint(2) + heightOfWindow * sin(pi/2 + slopeAngle);
    xRU = xRD + 2*widthOfWindow*cos(slopeAngle);
    yRU = yRD + 2*widthOfWindow*sin(slopeAngle);
    xLU = xLD + 2*widthOfWindow*cos(slopeAngle);
    yLU = yLD + 2*widthOfWindow*sin(slopeAngle);
elseif symmetric == 2
    xRD = startPoint(1) + 2 * heightOfWindow * cos(pi/2 + slopeAngle);
    yRD = startPoint(2) + 2 * heightOfWindow * sin(pi/2 + slopeAngle);
    xLD = startPoint(1) ;
    yLD = startPoint(2) ;
    xRU = xRD + 2*widthOfWindow*cos(slopeAngle);
    yRU = yRD + 2*widthOfWindow*sin(slopeAngle);
    xLU = xLD + 2*widthOfWindow*cos(slopeAngle);
    yLU = yLD + 2*widthOfWindow*sin(slopeAngle);
elseif symmetric == 1
    xRD = startPoint(1);
    yRD = startPoint(2);
    xLD = startPoint(1) + 2 * heightOfWindow * cos(-pi/2 + slopeAngle);
    yLD = startPoint(2) + 2 * heightOfWindow * sin(-pi/2 + slopeAngle);
    xRU = xRD + 2*widthOfWindow*cos(slopeAngle);
    yRU = yRD + 2*widthOfWindow*sin(slopeAngle);
    xLU = xLD + 2*widthOfWindow*cos(slopeAngle);
    yLU = yLD + 2*widthOfWindow*sin(slopeAngle);
end
% Construction of the window
xCornersTmp = [xRD, xRU, xLU, xLD,  xRD];
yCornersTmp = [yRD, yRU, yLU, yLD, yRD];
if all(xCornersTmp>=0) && all(yCornersTmp>=0)&& ~all(xCornersTmp == 0) && ~all(yCornersTmp == 0)
    xCorners = xCornersTmp;
    yCorners = yCornersTmp;
    borderKeeperFlag = false;
else
    %% find X & Y coordinates of the window corners
    
    % xRD, yRD: x & y of the Right-Down corner respectively
    % xLU, yLU: x & y of the Left-Upper corner respectively
    % and so on...
    if symmetric == 3
        xRD = startPoint(1) + breviousHeightOfWindow * cos(-pi/2 + slopeAngle);
        yRD = startPoint(2) + breviousHeightOfWindow * sin(-pi/2 + slopeAngle);
        xLD = startPoint(1) + breviousHeightOfWindow * cos(pi/2 + slopeAngle);
        yLD = startPoint(2) + breviousHeightOfWindow * sin(pi/2 + slopeAngle);
        xRU = xRD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yRU = yRD + 2*breviousWidthOfWindow*sin(slopeAngle);
        xLU = xLD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yLU = yLD + 2*breviousWidthOfWindow*sin(slopeAngle);
    elseif symmetric == 2
        xRD = startPoint(1) + 2 * breviousHeightOfWindow * cos(pi/2 + slopeAngle);
        yRD = startPoint(2) + 2 * breviousHeightOfWindow * sin(pi/2 + slopeAngle);
        xLD = startPoint(1) ;
        yLD = startPoint(2) ;
        xRU = xRD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yRU = yRD + 2*breviousWidthOfWindow*sin(slopeAngle);
        xLU = xLD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yLU = yLD + 2*breviousWidthOfWindow*sin(slopeAngle);
    elseif symmetric == 1
        xRD = startPoint(1);
        yRD = startPoint(2);
        xLD = startPoint(1) + 2 * breviousHeightOfWindow * cos(-pi/2 + slopeAngle);
        yLD = startPoint(2) + 2 * breviousHeightOfWindow * sin(-pi/2 + slopeAngle);
        xRU = xRD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yRU = yRD + 2*breviousWidthOfWindow*sin(slopeAngle);
        xLU = xLD + 2*breviousWidthOfWindow*cos(slopeAngle);
        yLU = yLD + 2*breviousWidthOfWindow*sin(slopeAngle);
    end
    % Construction of the window
    xCorners = [xRD, xRU, xLU, xLD,  xRD];
    yCorners = [yRD, yRU, yLU, yLD, yRD];
    borderKeeperFlag = true;
end
[numOfRows, numOfColumns] = size(robotMap);
window = poly2mask(yCorners, xCorners, numOfRows, numOfColumns);

end