function probabilisticMap = ProbabilisticMapConstruction()
global startP goalP alpha
global invMap numOfRows numOfColumns maxOccupation
[distance,slope] = DistanceBetweenEachPointAndLineSegement([numOfRows, numOfColumns],startP, goalP);
maxDist = max(distance(:));
probabilisticMap = alpha * (repmat(double(maxDist), numOfRows, numOfColumns) -...
    distance) ./ repmat(double(maxDist), numOfRows, numOfColumns) +...
    (1-alpha)* (repmat(double(maxOccupation), numOfRows, numOfColumns)-...
    double(invMap)) ./ repmat(double(maxOccupation), numOfRows, numOfColumns);
end