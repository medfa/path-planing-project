function angleInDegrees = CalculateAngle(p1,p2,p3)
% This function calculates the angle between two vectors
% The first vector connects p1 and p2
% The second vector connects p1 and p3

% Translate the vectors to the origin (0,0) instead of (p1(1),p1(2))
u = [p2(1)-p1(1) p2(2)-p1(2)];
v = [p3(1)-p1(1) p3(2)-p1(2)];

% Calculate the normalized angle between the two vectors
CosAngle = dot(u,v)/(norm(u)*norm(v));
angleInDegrees = acosd(CosAngle);

end