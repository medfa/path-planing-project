function [newStartPoint, previousPoint, isGoalArrived] =...
    AddCandidatePointToPath(robotMap, probabilisticMap, currentStartPoint,...
    previousPoint, startPoint, goalPoint,...
    probabilityThresholdOfEfficiencyInit)

global heightOfWindow widthOfWindow fireflies

probabilityThresholdOfEfficiency = probabilityThresholdOfEfficiencyInit;
slopeAngle = 90;
symmetric = 0; % symmetric window
helperCounter = 1;
xCorners = ones(1,5);
yCorners = ones(1,5);
[numOfRows, numOfColumns] = size(robotMap);
breviousHeightOfWindow = heightOfWindow;
breviousHeightOfWindow = widthOfWindow;
probabilityDensityIdx =  [];
[window, slopeAngle, xCorners, yCorners, ~] =...
    ConstructWindow(robotMap, currentStartPoint, startPoint, goalPoint,...
    symmetric, slopeAngle, xCorners, yCorners, heightOfWindow, widthOfWindow,breviousHeightOfWindow, breviousWidthOfWindow);% Construct the first window
% figure(5);
% imshow(window);
% drawnow
% hold on;

% apply the window on the probabilistic map
regionOfInterest =  probabilisticMap .* window;
% imshow(regionOfInterest);

isGoalArrived = regionOfInterest(goalPoint(1),goalPoint(2));
effeciencyOfROI = sum(sum(regionOfInterest));
numOfWindowCells = sum(sum(window));
expandingHeightCounter = 1;

%epsilon = pi;
epsilonFlag = false;
while epsilonFlag == false
    while effeciencyOfROI < (probabilityThresholdOfEfficiency*numOfWindowCells) || (length(find(regionOfInterest>0))==0)
        
        epsilonFlag=false;
        breviousHeightOfWindow = heightOfWindow;
        breviousHeightOfWindow = widthOfWindow;
        heightOfWindow = 1.2*heightOfWindow;% expand the height of the window
        
        if probabilityThresholdOfEfficiency < 0.7 * probabilityThresholdOfEfficiencyInit
            widthOfWindow = 1.3 * widthOfWindow;
        end
        
        % Construct the window
        [window, slopeAngle, xCorners, yCorners, borderKeeperFlag] =...
            ConstructWindow(robotMap, currentStartPoint, startPoint,...
            goalPoint, symmetric, slopeAngle, xCorners, yCorners,...
            heightOfWindow, widthOfWindow,breviousHeightOfWindow, breviousWidthOfWindow);
        
        if borderKeeperFlag == 1 ||...
                (probabilityThresholdOfEfficiency...
                <=0.3*probabilityThresholdOfEfficiencyInit...
                && regionOfInterest>0)
            
            break;
        end
        
        % apply the window(mask)on the probabilistic map
        regionOfInterest =  probabilisticMap .* window;
        %         imshow(regionOfInterest);
        %         drawnow;
        
        effeciencyOfROI = sum(sum(regionOfInterest));
        if isempty(effeciencyOfROI)
            continue
        end
        
        numOfWindowCells = sum(sum(window));
        expandingHeightCounter = expandingHeightCounter +1;
        
        if expandingHeightCounter >= 3
            probabilityThresholdOfEfficiency =...
                probabilityThresholdOfEfficiency * 8/10;
            expandingHeightCounter = 1;
        end
        
    end
    
    % Apply the Probability Density Function (PD) and Choose Random Point
    if isGoalArrived>0
        newStartPointIdx = sub2ind([numOfRows, numOfColumns], goalPoint);
        currentStartPoint = regionOfInterest(goalPoint(1),goalPoint(2));
        newStartPoint = newStartPointIdx;
        epsilonFlag = true;
        
    else
        normalizedProbability = regionOfInterest ./ effeciencyOfROI;
        minNormalizedProbability =...
            min(normalizedProbability(normalizedProbability>0));
        [sortedProbs, probsIdx] = sort(normalizedProbability(:),'descend');
        probabilityDensityRepetition =...
            sortedProbs ./ minNormalizedProbability;
        
        zeroIdx = find(sortedProbs==0,1);
        for i=1:zeroIdx-1
            %probabilityDensityFunction = cat(2,probabilityDensityFunction,repmat(sortedProbs(i),1, probabilityDensityRepetition(i)));
            probabilityDensityIdx = cat(1, probabilityDensityIdx,...
                (repmat(probsIdx(i), 1,...
                round(probabilityDensityRepetition(i))))');
        end
        
        while epsilonFlag==false
            Idx = find(randi(length(probabilityDensityIdx))<...
                cumsum(1:length(probabilityDensityIdx)),1);
            newStartPointIdx = probabilityDensityIdx(Idx);
            [newStartPointTemp(1),newStartPointTemp(2)] =...
                ind2sub([numOfRows, numOfColumns],...
                newStartPointIdx);
            if previousPoint == startPoint
                epsilon = mod(atan2(newStartPointTemp(2) -...
                    currentStartPoint(2),...
                    newStartPointTemp(1) - currentStartPoint(1)) -...
                    (slopeAngle), 2*pi);
            else
                epsilon =...
                    mod( atan2( det([newStartPointTemp(1) - currentStartPoint(1),...
                    currentStartPoint(1) - previousPoint(1);...
                    newStartPointTemp(2) - currentStartPoint(2),...
                    currentStartPoint(2) - previousPoint(2)]) ,...
                    dot([newStartPointTemp(1) - currentStartPoint(1),...
                    newStartPointTemp(2) - currentStartPoint(2)],...
                    [currentStartPoint(1) - previousPoint(1),...
                    currentStartPoint(2) - previousPoint(2)])), 2*pi );
                
            end
            
            helperCounter = helperCounter + 1;
            if ~(epsilon > pi/2 && epsilon < 3*pi/2)
                newStartPoint = newStartPointTemp;
                epsilonFlag = true;
                helperCounter = 1;
                disp(['epsilon = ' num2str(rad2deg(mod(epsilon,2*pi)))]);
                
            end
            
            if helperCounter>=5 
                probabilityThresholdOfEfficiency =...
                    max(probabilityThresholdOfEfficiency - 0.1,0.1);
                helperCounter = 1;
                break;
            end
        end
    end
end

previousPoint = currentStartPoint;

end