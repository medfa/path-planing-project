clc
close all;
clear all;

global startP goalP alpha probabilityThresholdOfEfficiency numOfFireflies
global coloredMap invMap numOfRows numOfColumns maxOccupation robotMap
global probabilisticMap initialFireflies heightOfWindow widthOfWindow fireflies

goalP =zeros(1,2);
alpha = 0.5;
probabilityThresholdOfEfficiency = 0.8;
numOfFirefliesInAutomatedMode = 3;
heightOfWindow = 20;
widthOfWindow = 20;
coloredMap = imread('PathPlanningMap.png');
grayMap = rgb2gray(coloredMap);
robotMap = imresize(grayMap, 1);
invMap = imcomplement(robotMap);
[numOfRows, numOfColumns] = size(invMap); %size of the map

%%% Start and Goal Point Selecting
figure(1);
imshow(coloredMap);
hold on;
disp('Choose your start Point');
[yStartC, xStartC, but] = ginput(1);
h1 = text(yStartC,xStartC,'START', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xStart = round(xStartC);
yStart = round(yStartC);
startP = [xStartC, yStartC];
% Validation test of Start Point
if invMap(xStart,yStart) > 0
    disp('You chose not Valid Start Point, Try again!');
    return;
end
disp('Choose your Goal Point');
[yGoalC, xGoalC, but2] = ginput(1);      % get a point
h2 = text(yGoalC,xGoalC,'GOAL', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xGoal = round(xGoalC);
yGoal = round(yGoalC);
goalP = [xGoal, yGoal];

% Validation test of Goal Point
if invMap(xGoal,yGoal) > 0
    disp('You chose unavailable Start Point, Try again!');
    return;
end

% manual or automated mode
% manual mode: you eill be able to construct your own two paths (JUST
% TWO!).
% automated mode (recommended): paths will be generated automaticly
disp('Manual or Automated Mode');
disp('Manual Mode: You will be able to construct your own two paths (JUST TWO!).')
disp('Automated Mode (recommended): Paths will be generated automaticly!');
chosenMode = input('ENTER "0" for Automated Mode or "1" for Manual Mode:   ');
% chosenMode = 1;
% Manual Mode
if chosenMode == 1
    numOfFireflies = 2;
    initialFireflies{numOfFireflies} = [];
    fireflies{numOfFireflies} = [];
    %%% Choose points of PATH A and PATH B
    disp('Note: Please Enter the Number Whithout Considering the Start and Goal Points!')
    numOfPathAPoints = input('Enter Number Of Points that Consists the PATH A:	');
    numOfPathBPoints = input('Enter Number Of Points that Consists the PATH B:	');
    for i= 1:numOfPathAPoints
        disp(['Choose the Point ' num2str(i)]);
        [yC, xC, but3] = ginput(1);      % get a point
        h2 = text(yC,xC,'*', ...
            'HorizontalAlignment','center', ...
            'Color', [0 1 0], ...
            'FontSize',14);
        x = round(xC);
        y = round(yC);
        if isempty(initialFireflies{1})
            initialFireflies{1} =...
                [startP(1);startP(2)];
        end
        initialFireflies{1} =...
            cat(2, initialFireflies{1},[xC;yC]);
        if i==numOfPathAPoints
            initialFireflies{1} =...
                cat(2, initialFireflies{1},[goalP(1);goalP(2)]);
        end
    end
    
    for i= 1:numOfPathBPoints
        disp(['Choose the Point ' num2str(i)]);
        [yC, xC, but3] = ginput(1);      % get a point
        h2 = text(yC,xC,'*', ...
            'HorizontalAlignment','center', ...
            'Color', [0 0 1], ...
            'FontSize',14);
        x = round(xC);
        y = round(yC);
        if isempty(initialFireflies{2})
            initialFireflies{2} =...
                [startP(1);startP(2)];
        end
        initialFireflies{2} =...
            cat(2, initialFireflies{2},[xC;yC]);
        
        if i==numOfPathBPoints
            initialFireflies{2} =...
                cat(2, initialFireflies{2},[goalP(1);goalP(2)]);
        end
    end
end

%%% Probabilistic Map
maxOccupation = max(invMap(:));
probabilisticMap = ProbabilisticMapConstruction;

% hold off;
% figure(3);
% imshow(probabilisticMap);

% Automated Mode
if chosenMode ~= 1
    %%% Initilization of fireflies
    
    numOfFireflies = numOfFirefliesInAutomatedMode;
    initialFirefliesTotally{numOfFireflies} = [];
    fireflies{numOfFireflies} = [];
    initialFirefliesTotally = GenerateInitialFirefliesPopulation(numOfFireflies,...
        invMap, probabilisticMap,  startP, goalP,...
        probabilityThresholdOfEfficiency);
    solSel = mod(randi, numOfFireflies);
    initialFireflies = initialFirefliesTotally(:,[solSel,mod(solSel+randi, numOfFireflies)]); 
end
% figure(6);
% imshow(grayMap);
% hold on;
% for i= 1:numOfFireflies
%     rng(i);
%     color = rand(3);
%     %plot(fireflies{i}(1,:),fireflies{i}(2,:),'*','color',rand(1,3));
%     text(fireflies{i}(2,:),fireflies{i}(1,:),'*', ...
%                 'HorizontalAlignment','center', ...
%                 'Color', [color(1) color(2) color(3)], ...
%                 'FontSize',14);
% end
% hold off;

%%% Generate New Path From Two Paths
path = GenerateNewPathFromTwoPaths;